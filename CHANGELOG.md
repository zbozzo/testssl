## [3.1.1](https://gitlab.com/to-be-continuous/testssl/compare/3.1.0...3.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([511416e](https://gitlab.com/to-be-continuous/testssl/commit/511416eb0c0feca5541fa45a7001d8699d9faddb))

# [3.1.0](https://gitlab.com/to-be-continuous/testssl/compare/3.0.0...3.1.0) (2022-10-04)


### Features

* normalize reports ([65a1531](https://gitlab.com/to-be-continuous/testssl/commit/65a1531fd9d34a3871b1874fe5c6dd492ec1ec39))

# [3.0.0](https://gitlab.com/to-be-continuous/testssl/compare/2.1.0...3.0.0) (2022-08-05)


### Features

* adapative pipeline ([aa79e64](https://gitlab.com/to-be-continuous/testssl/commit/aa79e642d08722380968cc76d4956928b767bdbd))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.1.0](https://gitlab.com/to-be-continuous/testssl/compare/2.0.1...2.1.0) (2022-05-01)


### Features

* configurable tracking image ([fb2f861](https://gitlab.com/to-be-continuous/testssl/commit/fb2f8616b5c3ee2146af68b710f9e1cd7dfe2ad9))

## [2.0.1](https://gitlab.com/to-be-continuous/testssl/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([7dcbf20](https://gitlab.com/to-be-continuous/testssl/commit/7dcbf203a8332f6e3da55013e8a010fa070cad85))

## [2.0.0](https://gitlab.com/to-be-continuous/testssl/compare/1.2.0...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([f5ada39](https://gitlab.com/to-be-continuous/testssl/commit/f5ada3929ff69c15017a3d42deb736662bcb7500))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.0](https://gitlab.com/to-be-continuous/testssl/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([9aa49ec](https://gitlab.com/to-be-continuous/testssl/commit/9aa49ecb2fd75099e09d5fe957b6d111fdb8aca6))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/testssl/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([b02c22a](https://gitlab.com/Orange-OpenSource/tbc/testssl/commit/b02c22a0ad8dea173023da137269a3c728bc8ebe))

## 1.0.0 (2021-05-06)

### Features

* initial release ([ed6a90c](https://gitlab.com/Orange-OpenSource/tbc/testssl/commit/ed6a90cd3be63c2c84b4069486112b62d3caca36))
