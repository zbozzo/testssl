# GitLab CI template for Test SSL

This project implements a GitLab CI/CD template to test your TLS/SSL servers compliance with [Test SSL](https://testssl.sh/).

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/testssl'
    ref: '3.1.1'
    file: '/templates/gitlab-ci-testssl.yml'
```

:warning: this job do not fail unless there is a technical problem while scanning your endpoint. This means you have to read the tool report on gitlab or download the report to properly assert if the security level of your endpoint is correct. You can use [DTSI variant](#dtsi-variant) which will fail on non-compliance with DTSI rules.

## `testssl` job

This job performs a TLS/SSL compliancy analysis on a given server.

It uses the following variable:

| Name            | description                              | default value     |
| --------------- | ---------------------------------------- | ----------------- |
| `TESTSSL_IMAGE` | The Docker image used to run [Test SSL](https://testssl.sh/) | `registry.hub.docker.com/drwetter/testssl.sh:latest` |
| `TESTSSL_ARGS`  | Test SSL [command-line options](https://testssl.sh/#usage)   | `--severity MEDIUM` |
| `TESTSSL_URL`   | Server url to test TLS/SSL against       | _none_ (auto evaluated: see below) |
| `REVIEW_ENABLED`| Set to `true` to enable Test SSL tests on review environments (dynamic environments instantiated on development branches) | _none_ (disabled) |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `reports/testssl.native.json` | testssl.sh JSON format | [DefectDojo integration](https://defectdojo.github.io/django-DefectDojo/integrations/parsers/#testssl-scan) |

### test url auto evaluation

By default, the Test SSL template tries to auto-evaluates the server url to test by looking either for a
`$environment_url` variable or for an `environment_url.txt` file.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv) variable `$environment_url`
or through a basic `environment_url.txt` file, then the Test SSL will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your Test SSL tests.

If you're not using a smart deployment job, you may still explicitly declare the `TESTSSL_URL` variable (but that
will be unfortunately hardcoded to a single server).
